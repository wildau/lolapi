from enum import Enum

class EloSortEnum(Enum):
    HIGH = 1
    LOW = 2
    EVEN = 3
    NEW = 4