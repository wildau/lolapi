http://h2434740.stratoserver.net:5984/lolapi/_design/testDesign/_view/test-view?group=true

function (doc) {
  for (var index in doc.participantIdentities){
    var player = doc.participantIdentities[index].player;
    var accountId = player.currentAccountId;
    emit(accountId, 1);
  }
}
_count

http://h2434740.stratoserver.net:5984/lolapi/_design/testestDesign/_view/liga-account-view?group=true
http://h2434740.stratoserver.net:5984/lol_api_euw/_design/design/_view/account-view?group=true

function (doc) {
  liga = doc.eloString
  for (var index in doc.participantIdentities){
    var player = doc.participantIdentities[index].player;
    var accountId = player.currentAccountId;
    emit([liga, accountId], 1);
  }
}
_count

FOLGENDES WIRD VERWENDET:
http://h2434740.stratoserver.net:5984/lol_api_test/_design/design/_view/liga-view?group=true

function (doc) {
  emit(doc.eloString, 1);
}
_count

FOLGENDES WIRD VERWENDET:
http://h2434740.stratoserver.net:5984/lol_api_test/_design/design/_view/exporter-view

function (doc) {
  if (doc.timeline == false) return;//Weil manche Spiele keine timeline haben

  values = {};

  values['win'] = doc.teams[0].win == 'Win';
  values['eloString'] = doc.eloString;
  values['eloFloat'] = doc.eloFloat;
  values['firstTower'] = doc.teams[0].firstTower;
  values['firstDragon'] = doc.teams[0].firstDragon;
  values['firstBlood'] = doc.teams[0].firstBlood;
  values['countTowers'] = [0, 0];
  values['countDragons'] = [0, 0];
  values['countRiftHeralds'] = [0, 0];
  values['countInhibitors'] = [0, 0];

  values['participants'] = [];
  for (var index in doc.participants){
    participant = doc.participants[index];
    map = {};

    map['championId'] = participant.championId;
    map['spell1Id'] = participant.spell1Id;
    map['spell2Id'] = participant.spell2Id;
    map['role'] = participant.timeline.role;
    map['lane'] = participant.timeline.lane;
    map['xpPerMinDelta'] = participant.timeline.xpPerMinDeltas['10-20'];
    map['goldPerMinDelta'] = participant.timeline.goldPerMinDeltas['10-20'];
    map['creepsPerMinDelta'] = participant.timeline.creepsPerMinDeltas['10-20'];
    map['kills'] = 0;
    map['assists'] = 0;
    map['deaths'] = 0;
	map['yellowTrinket'] = 0;
	map['blueTrinket'] = 0;
	map['controlWard'] = 0;
	map['wardsKilled'] = 0;

    values['participants'][participant.participantId - 1] = map;
  }
  
  maxSeconds = 20 * 60 * 1000;//20 Minuten
  for (var index in doc.timeline.frames){
    frame = doc.timeline.frames[index];
    
    for (var eventIndex in frame.events){
      event = frame.events[eventIndex];
      type = event.type;
      
      if (type == 'CHAMPION_KILL'){
        values['participants'][event.killerId - 1]['kills'] += 1;
        values['participants'][event.victimId - 1]['deaths'] += 1;
        for (var i in event.assistingParticipantIds){
          assistId = event.assistingParticipantIds[i]
          values['participants'][assistId - 1]['assists'] += 1;
        }
      } else if (type == 'ELITE_MONSTER_KILL'){
        multiplier = 0;
        if (event.killerId > 5){
          multiplier = 1;
        }
        if (event.monsterType == 'DRAGON'){
          values['countDragons'][multiplier] += 1;
        } else if (event.monsterType == 'RIFTHERALD'){
          values['countRiftHeralds'][multiplier] += 1;
        }
      } else if (type == 'BUILDING_KILL'){
        multiplier = 1;
        if (event.teamId == doc.teams[1].teamId){//Es steht immer die TeamId welche den Tower verloren hat, ich vermute dass bei Inhibs genauso ist
          multiplier = 0;
        }
        if (event.buildingType == 'TOWER_BUILDING'){
          values['countTowers'][multiplier] += 1;
        } else if (event.buildingType == 'INHIBITOR_BUILDING'){
          values['countInhibitors'][multiplier] += 1;
        }
      } else if (event.type == 'WARD_PLACED'){
		if (event.wardType == 'YELLOW_TRINKET'){
			values['participants'][event.creatorId - 1]['yellowTrinket'] += 1;
		} else if (event.wardType == 'CONTROL_WARD'){
			values['participants'][event.creatorId - 1]['controlWard'] += 1;
		} else if (event.wardType == 'SIGHT_WARD'){
			values['participants'][event.creatorId - 1]['yellowTrinket'] += 1;
		} else if (event.wardType == 'BLUE_TRINKET'){
			values['participants'][event.creatorId - 1]['blueTrinket'] += 1;
		} else if (event.wardType == 'UNDEFINED'){
			//TODO was ist mit denen??? wahrscheinlich scuttle crab
		}
	  } else if (event.type == 'WARD_KILL'){
		values['participants'][event.killerId - 1]['wardsKilled'] += 1;
	  }
    }
    
    if (frame.timestamp >= maxSeconds){
      for (var innerIndex in frame.participantFrames){
        participantFrame = frame.participantFrames[innerIndex];
        participantId = participantFrame.participantId;
        map = values['participants'][participantId - 1];
        
        map['level'] = participantFrame.level;
        map['jungleMinionsKilled'] = participantFrame.jungleMinionsKilled;
        map['totalGold'] = participantFrame.totalGold;
        map['minionsKilled'] = participantFrame.minionsKilled;
      }
      break;
    }
  }

  emit(doc.gameId, values);
}

FOLGENDES WIRD VERWENDET:
http://h2434740.stratoserver.net:5984/lol_api_test/_design/design/_view/exporter-view-v2

function (doc) {
  if (doc.timeline == false) return;//Weil manche Spiele keine timeline haben
  minuteArray = [5, 10, 15, 20, 25, 30, 40, 50];
  
  values = {};

  values['gameDurationInMin'] = doc.gameDuration / 60.0;
  values['win'] = doc.teams[0].win == 'Win';
  values['eloString'] = doc.eloString;
  values['eloFloat'] = doc.eloFloat;
  values['firstTower'] = doc.teams[0].firstTower == false && doc.teams[1].firstTower == false ? 'null' : (doc.teams[0].firstTower ? 'blue' : 'red');
  values['firstDragon'] = doc.teams[0].firstDragon == false && doc.teams[1].firstDragon == false ? 'null' : (doc.teams[0].firstDragon ? 'blue' : 'red');
  values['firstBlood'] = doc.teams[0].firstBlood == false && doc.teams[1].firstBlood == false ? 'null' : (doc.teams[0].firstBlood ? 'blue' : 'red');
  values['countTowers'] = [{}, {}];
  values['countDragons'] = [{}, {}];
  values['countRiftHeralds'] = [{}, {}];
  values['countInhibitors'] = [{}, {}];
  
	for (var minuteArrayIndex in minuteArray){
		var minute = minuteArray[minuteArrayIndex];
		
		for (var team_id = 0; team_id < 2; team_id++){
			values['countTowers'][team_id][minute] = 0;
			values['countDragons'][team_id][minute] = 0;
			values['countRiftHeralds'][team_id][minute] = 0;
			values['countInhibitors'][team_id][minute] = 0;
		}
	}
  
  var searchForKeyAndGetValue = function(arr){
	  var keys = ['10-20', '0-10'];
	  
	  for (var i in keys){
		  key = keys[i];
		  
		  endKey = key.split('-')[0] + '-end';
		  if (endKey in arr){
			  return arr[endKey];
		  }
		  if (key in arr){
			  return arr[key];
		  }
	  }
  }

  values['participants'] = [];
  for (var index in doc.participants){
    participant = doc.participants[index];
    map = {};

    map['championId'] = participant.championId;
    map['spell1Id'] = participant.spell1Id;
    map['spell2Id'] = participant.spell2Id;
    map['role'] = participant.timeline.role;
    map['lane'] = participant.timeline.lane;
    map['xpPerMinDelta'] = searchForKeyAndGetValue(participant.timeline.xpPerMinDeltas);
    map['goldPerMinDelta'] = searchForKeyAndGetValue(participant.timeline.goldPerMinDeltas);
    map['creepsPerMinDelta'] = searchForKeyAndGetValue(participant.timeline.creepsPerMinDeltas);
	
	map['kills'] = {};
	map['assists'] = {};
	map['deaths'] = {};
	map['yellowTrinket'] = {};
	map['blueTrinket'] = {};
	map['controlWard'] = {};
	map['wardsKilled'] = {};
	//Die nächsten 4 müssten hier nicht stehen, aber ich habe sie gerne initialisiert für den Exporter
	map['level'] = {};
	map['jungleMinionsKilled'] = {};
	map['totalGold'] = {};
	map['minionsKilled'] = {};
	for (var minuteArrayIndex in minuteArray){
		var minute = minuteArray[minuteArrayIndex];
		
		map['kills'][minute] = 0;
		map['assists'][minute] = 0;
		map['deaths'][minute] = 0;
		map['yellowTrinket'][minute] = 0;
		map['blueTrinket'][minute] = 0;
		map['controlWard'][minute] = 0;
		map['wardsKilled'][minute] = 0;
		//Die nächsten 4 müssten hier nicht stehen, aber ich habe sie gerne initialisiert für den Exporter
		map['level'][minute] = 0;
		map['jungleMinionsKilled'][minute] = 0;
		map['totalGold'][minute] = 0;
		map['minionsKilled'][minute] = 0;
	}

    values['participants'][participant.participantId - 1] = map;
  }
  
  for (var index in doc.timeline.frames){
    frame = doc.timeline.frames[index];
	
	var minuteKey = minuteArray[0];
	var lastIndexReached = false;
	for (var minuteArrayIndex in minuteArray){
		var minute = minuteArray[minuteArrayIndex];
		var seconds = minute * 60 * 1000;
		if (frame.timestamp < seconds){
			break;
		}
		minuteKey = minute;
		if (minuteArrayIndex + 1 == minuteArray.length){
			lastIndexReached = true;
		}
	}
    
    for (var eventIndex in frame.events){
      event = frame.events[eventIndex];
      type = event.type;
      
      if (type == 'CHAMPION_KILL'){
        values['participants'][event.killerId - 1]['kills'][minuteKey] += 1;
        values['participants'][event.victimId - 1]['deaths'][minuteKey] += 1;
        for (var i in event.assistingParticipantIds){
          assistId = event.assistingParticipantIds[i]
          values['participants'][assistId - 1]['assists'][minuteKey] += 1;
        }
      } else if (type == 'ELITE_MONSTER_KILL'){
        multiplier = 0;
        if (event.killerId > 5){
          multiplier = 1;
        }
        if (event.monsterType == 'DRAGON'){
          values['countDragons'][multiplier][minuteKey] += 1;
        } else if (event.monsterType == 'RIFTHERALD'){
          values['countRiftHeralds'][multiplier][minuteKey] += 1;
        }
      } else if (type == 'BUILDING_KILL'){
        multiplier = 1;
        if (event.teamId == doc.teams[1].teamId){//Es steht immer die TeamId welche den Tower verloren hat, ich vermute dass bei Inhibs genauso ist
          multiplier = 0;
        }
        if (event.buildingType == 'TOWER_BUILDING'){
          values['countTowers'][multiplier][minuteKey] += 1;
        } else if (event.buildingType == 'INHIBITOR_BUILDING'){
          values['countInhibitors'][multiplier][minuteKey] += 1;
        }
      } else if (event.type == 'WARD_PLACED'){
		if (event.wardType == 'YELLOW_TRINKET'){
			values['participants'][event.creatorId - 1]['yellowTrinket'][minuteKey] += 1;
		} else if (event.wardType == 'CONTROL_WARD'){
			values['participants'][event.creatorId - 1]['controlWard'][minuteKey] += 1;
		} else if (event.wardType == 'SIGHT_WARD'){
			values['participants'][event.creatorId - 1]['yellowTrinket'][minuteKey] += 1;
		} else if (event.wardType == 'BLUE_TRINKET'){
			values['participants'][event.creatorId - 1]['blueTrinket'][minuteKey] += 1;
		} else if (event.wardType == 'UNDEFINED'){
			//TODO was ist mit denen??? wahrscheinlich scuttle crab
		}
	  } else if (event.type == 'WARD_KILL'){
		values['participants'][event.killerId - 1]['wardsKilled'][minuteKey] += 1;
	  }
    }
    
	for (var innerIndex in frame.participantFrames){
		participantFrame = frame.participantFrames[innerIndex];
		participantId = participantFrame.participantId;
		map = values['participants'][participantId - 1];

		map['level'][minuteKey] = participantFrame.level;
		map['jungleMinionsKilled'][minuteKey] = participantFrame.jungleMinionsKilled;
		map['totalGold'][minuteKey] = participantFrame.totalGold;
		map['minionsKilled'][minuteKey] = participantFrame.minionsKilled;
	}
		
	if (lastIndexReached && frame.timestamp >= minuteKey * 60 * 1000){
		break;
	}
  }
  
  //Aufaddieren der Werte, da sonst nicht die Werte der vorherigen Minuten enthalten sind
	for (var minuteArrayIndex = 1; minuteArrayIndex < minuteArray.length; minuteArrayIndex++){
		var minute = minuteArray[minuteArrayIndex];
		var iterMinute = minuteArray[minuteArrayIndex-1];
		
		for (var team_id = 0; team_id < 2; team_id++){
			values['countTowers'][team_id][minute] += values['countTowers'][team_id][iterMinute];
			values['countDragons'][team_id][minute] += values['countDragons'][team_id][iterMinute];
			values['countRiftHeralds'][team_id][minute] += values['countRiftHeralds'][team_id][iterMinute];
			values['countInhibitors'][team_id][minute] += values['countInhibitors'][team_id][iterMinute];
		}
		
		for (var participantIndex = 0; participantIndex < 10; participantIndex++){
			map = values['participants'][participantIndex];
			
			map['kills'][minute] += map['kills'][iterMinute];
			map['assists'][minute] += map['kills'][iterMinute];
			map['deaths'][minute] += map['kills'][iterMinute];
			map['yellowTrinket'][minute] += map['kills'][iterMinute];
			map['blueTrinket'][minute] += map['kills'][iterMinute];
			map['controlWard'][minute] += map['kills'][iterMinute];
			map['wardsKilled'][minute] += map['kills'][iterMinute];
		}
	}
  
  //PLAYER POSITIONS
	var playerPositions = {};
	for (var id = 1; id <= 10; id++){
		playerPositions[id] = [];
	}

	//Normale Positionen UND Tode
	for (var frameIndex in doc.timeline.frames){
		frame = doc.timeline.frames[frameIndex];
		timestamp = frame.timestamp;
		
		for (var parFrameIndex in frame.participantFrames){
			participantFrame = frame.participantFrames[parFrameIndex];
			participantID = participantFrame.participantId;
			
			if (('position' in participantFrame) == false){
				break;
			}
			
			playerPositions[participantID].push({
				'x': participantFrame.position.x,
				'y': participantFrame.position.y,
				'timestamp': timestamp
			});
		}
		
		//Tode
		for (var eventIndex in frame.events){
			event = frame.events[eventIndex];
			type = event.type;

			if (type == 'CHAMPION_KILL'){
				participantID = event.victimId;
				playerPositions[participantID].push({
					'x': event.position.x,
					'y': event.position.y,
					'timestamp': timestamp
				});
			}
		}
	}
	
	//Ermittlung der eigentlichen Positionen
	var topLaneData = [[0, 5500], [2300, 5500], [2300, 10000], [4500, 12500], [9300, 12500], [9300, 14800], [0, 14800]];
	var botLaneData = [];
	for (var iii in topLaneData){
		botLaneData.push([topLaneData[iii][1], topLaneData[iii][0]]);
	}
	var midLaneData = [[3500, 5500], [5500, 3500], [11300, 9300], [9300, 11300]];
	var blueBaseData = [topLaneData[0], midLaneData[0], midLaneData[1], botLaneData[0], [0, 0]];
	var redBaseData = [];
	for (var iiii in blueBaseData){
		redBaseData.push([14800 - blueBaseData[iiii][1], 14800 - blueBaseData[iiii][0]]);
	}
	
	var minMilliSeconds = 1 * 60 * 1000;  // 1 Minute
    var maxMilliSeconds = 16 * 60 * 1000; // 16 Minuten
	
	var calculatedRoles = {};
	var rolePositionMap = {};
	for (var id = 1; id <= 10; id++){
		calculatedRoles[id] = 'ERROR';
		rolePositionMap[id] = [];
	}

	var TOP = 'TOP';
	var MID = 'MIDDLE';
	var BOT = 'BOTTOM';
	var JUNGLE = 'JUNGLE';
	
	//Function Definition isInPolygon
	var isInPolygon = function(polygonArray, testx, testy){
        var vertx = [];
        var verty = [];
		for (var i in polygonArray){
			vertx.push(polygonArray[i][0]);
			verty.push(polygonArray[i][1]);
		}

        var nvert = polygonArray.length;
        var i;
		var j;
		var c = false;
        for (i = 0, j = nvert-1; i < nvert; j = i++) {
            if ( ((verty[i]>testy) != (verty[j]>testy)) &&
            (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
                c = !c;
        }
        return c;
    }

	for (var id = 1; id <= 10; id++){
		var positionCountMap = {};
		positionCountMap[TOP] = 0;
		positionCountMap[MID] = 0;
		positionCountMap[BOT] = 0;
		positionCountMap[JUNGLE] = 0;

		for (var playerPositionsIndex in playerPositions[id]){
			var positionData = playerPositions[id][playerPositionsIndex];
			var timestamp = positionData['timestamp'];
			
			if (minMilliSeconds <= timestamp && timestamp <= maxMilliSeconds){
				var x = positionData['x'];
				var y = positionData['y'];
				if (isInPolygon(blueBaseData, x, y) || isInPolygon(redBaseData, x, y)){
					continue;
				}
				if (isInPolygon(topLaneData, x, y)){
					positionCountMap[TOP] += 1;
					continue;
				}
				if (isInPolygon(midLaneData, x, y)){
					positionCountMap[MID] += 1;
					continue;
				}
				if (isInPolygon(botLaneData, x, y)){
					positionCountMap[BOT] += 1;
					continue;
				}
				positionCountMap[JUNGLE] += 1;
			}
		}
		rolePositionMap[id] = positionCountMap;
	}
	
	for (var id in rolePositionMap){
		var positionCountMap = rolePositionMap[id];
		calculatedRoles[id] = Object.keys(positionCountMap).reduce(function(a, b){ return positionCountMap[a] > positionCountMap[b] ? a : b });
	}
	
	//CS-Zahlen ermitteln
	var csPerPlayer = {};
	for (var id = 1; id <= 10; id++){
		csPerPlayer[id] = values['participants'][id-1]['creepsPerMinDelta'];
	}
	
	var solveConflicts = function(range, calculatedRoles, rolePositionMap, BOT, recCounter){
		recCounter += 1;
		var roles = []
		for(var index in range){
			var id = range[index];
			roles.push(calculatedRoles[id]);
		}
		
		var counts = {};
		for (var i = 0; i < roles.length; i++) {
			var num = roles[i];
			counts[num] = counts[num] ? counts[num] + 1 : 1;
		}
		
        for (var role in counts){
			var count = counts[role];
            if (role == BOT){
                if (count > 2){
					if (recCounter > 1){
						return false;
					}
					var participantID_1 = -1;
					var participantID_2 = -1;
					var participantID_3 = -1;
					
					for(var index in range){
						var id = range[index];
						if (calculatedRoles[id] == role){
							if (participantID_1 === -1){
								participantID_1 = id;
							} else {
								if (participantID_2 === -1){
									participantID_2 = id;
								} else {
									participantID_3 = id;
								}
							}
						}
					}
					
					var loser_id = -1;
					if (rolePositionMap[participantID_1][role] < rolePositionMap[participantID_2][role]){
						loser_id = participantID_1;
					} else {
						loser_id = participantID_2;
					}
					
					if (rolePositionMap[loser_id][role] < rolePositionMap[participantID_3][role]){
						loser_id = loser_id;
					} else {
						loser_id = participantID_3;
					}
					
					var secondHighestRole;
					var secondHighestRoleCount = -1;
					for (var positionCountMapRole in rolePositionMap[loser_id]){
						var countOfPosition = rolePositionMap[loser_id][positionCountMapRole];
						if (positionCountMapRole != role){
							if (secondHighestRoleCount < countOfPosition){
								secondHighestRole = positionCountMapRole;
								secondHighestRoleCount = countOfPosition;
							}
						}
					}
					calculatedRoles[loser_id] = secondHighestRole;
					
                    return solveConflicts(range, calculatedRoles, rolePositionMap, BOT, recCounter);
                }
            } else {
                if (count > 1){
					if (recCounter > 1){
						return false;
					}
					var participantID_1 = -1;
					var participantID_2 = -1;
					
					for(var index in range){
						var id = range[index];
						if (calculatedRoles[id] == role){
							if (participantID_1 === -1){
								participantID_1 = id;
							} else {
								participantID_2 = id;
							}
						}
					}
					
					var loser_id = -1;
					if (rolePositionMap[participantID_1][role] < rolePositionMap[participantID_2][role]){
						loser_id = participantID_1;
					} else {
						loser_id = participantID_2;
					}
					
					var secondHighestRole;
					var secondHighestRoleCount = -1;
					for (var positionCountMapRole in rolePositionMap[loser_id]){
						var countOfPosition = rolePositionMap[loser_id][positionCountMapRole];
						if (positionCountMapRole != role){
							if (secondHighestRoleCount < countOfPosition){
								secondHighestRole = positionCountMapRole;
								secondHighestRoleCount = countOfPosition;
							}
						}
					}
					calculatedRoles[loser_id] = secondHighestRole;
					
                    return solveConflicts(range, calculatedRoles, rolePositionMap, BOT, recCounter);
                }
            }
        }
		
        return true;
    };
	
	//Konflikte lösen
	var success = solveConflicts([1,2,3,4,5], calculatedRoles, rolePositionMap, BOT, 0);
	success = success && solveConflicts([6,7,8,9,10], calculatedRoles, rolePositionMap, BOT, 0);
	
	values['rolesAreCorrect'] = success;

	var differentiateSupportFromADC = function(range, calculatedRoles, csPerPlayer, BOT){
        var id_1 = -1;
        var id_2;
        for (var index in range){
			var id = range[index];
            if (calculatedRoles[id] == BOT){
                if (id_1 !== -1){
                    id_2 = id;
                } else {
                    id_1 = id;
                }
            }
        }

        if (csPerPlayer[id_1] > csPerPlayer[id_2]){
            calculatedRoles[id_1] += '_ADC';
            calculatedRoles[id_2] += '_SUPPORT';
        } else {
            calculatedRoles[id_2] += '_ADC';
            calculatedRoles[id_1] += '_SUPPORT';
        }
    }
	
	//Support von ADC unterscheiden
	differentiateSupportFromADC([1,2,3,4,5], calculatedRoles, csPerPlayer, BOT);
	differentiateSupportFromADC([6,7,8,9,10], calculatedRoles, csPerPlayer, BOT);
	
	for (var id = 1; id <= 10; id++){
		values['participants'][id - 1]['calculatedRole'] = calculatedRoles[id];
	}

  emit(doc.gameId, values);
}