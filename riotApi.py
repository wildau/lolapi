import urllib.request
import json
import urllib.error
import datetime
import time
import sys, os
from myTokenBucket import MyTokenBucket
import threading
import constant
import logging

class RiotApi:
    _lock = threading.Lock()
    _invalidApiKeys = []
    _apiKeys = None  # Nicht verwenden

    @staticmethod
    def __readApiKeys():
        with open(os.path.dirname(__file__) + '/api-key.txt', 'r') as f:
            content = f.readlines()
        content = [x.strip() for x in content]
        RiotApi._apiKeys = []
        for val in content:
            val = val.split('#')[0]
            if (val not in RiotApi._invalidApiKeys):
                RiotApi._apiKeys.append([val, MyTokenBucket()])

    @staticmethod
    def __getApiKeys():
        if (RiotApi._apiKeys is None):
            RiotApi.__readApiKeys()
        if (len(RiotApi._apiKeys) <= 0):
            logging.critical('Keine gueltigen Api-Keys vorhanden!')
            sys.exit('Keine gueltigen Api-Keys vorhanden!')
        return RiotApi._apiKeys

    @staticmethod
    def __readUrl(url, appendQuestionMark = True):
        if appendQuestionMark:
            url += '?'
        else:
            url += '&'

        counter = 0
        sleepTimes = [1, 2, 4, 8, 12, 20, 60, 120, 360]
        while True:
            if counter >= len(sleepTimes):
                counter = len(sleepTimes) - 1


            apiKey = None
            while apiKey is None:
                sleepy = None
                with RiotApi._lock:
                    secondsToSleep = None
                    for pair in RiotApi.__getApiKeys():
                        secondsTillNextConsume = pair[1].secondsTillNextConsume()
                        if secondsToSleep is None or secondsTillNextConsume < secondsToSleep:
                            secondsToSleep = secondsTillNextConsume

                        if secondsTillNextConsume <= 0:
                            apiKey = pair[0]
                            pair[1].consume()
                            bucket = pair[1]
                            break
                    if apiKey is None:
                        sleepy = 0 if secondsToSleep is None else secondsToSleep
                if sleepy is not None:
                    logging.debug('Sleeping ' + str(round(sleepy)) + ' seconds')
                    time.sleep(sleepy)

            newUrl = url + 'api_key=' + apiKey
            try:
                #content = open('test.json', 'r')#DEBUG
                #return json.load(content)["matches"][0]#DEBUG
                content = urllib.request.urlopen(newUrl).read().decode('utf-8')
                return json.loads(content)
            except urllib.error.HTTPError as exc:
                logging.debug('Error catching ' + str(exc.code))
                if exc.code == 429:
                    sleepTime = exc.headers['Retry-After']
                    #print('Sleepy ' + sleepTime + ' Seconds')
                    #time.sleep(int(sleepTime))
                    logging.warning('Zu viele Anfragen! Soll warten ' + (sleepTime if sleepTime is not None else 'FEHLER(UNBESTIMMT)') + ' Sekunden')
                    with RiotApi._lock:
                        bucket.fillUp()
                else:
                    if exc.code == 401 or exc.code == 403:
                        # Key ist ungültig
                        RiotApi._invalidApiKeys.append(apiKey)
                        RiotApi._apiKeys = None
                    else:
                        if exc.code == 422 or exc.code == 404:
                            # keine daten vorhanden
                            return False
                        else:
                            if exc.code == 500 or exc.code == 502 or exc.code == 503 or exc.code == 504:
                                # Service unavailable
                                time.sleep(sleepTimes[counter])
                                counter += 1
                                continue
                            else:
                                logging.critical(exc)
                                sys.exit(exc)
            except urllib.error.URLError as exc:
                # Für die Timeout Exception
                time.sleep(sleepTimes[counter])
                counter += 1
                continue
            counter = 0
        return None

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getAllMatches(accountId, region = 'euw1'):
        epoch = datetime.datetime.utcfromtimestamp(0)
        endTime = datetime.datetime.now()
        beginTime = endTime + datetime.timedelta(days=-7)
        endTime = int(round(time.time() * 1000))
        beginTime = (int)((beginTime - epoch).total_seconds() * 1000.0)
        # Hier könnte man noch einen Zeitintervall angegeben den man abgefragt haben will beginTime & endTime
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/match/v3/matchlists/by-account/' + str(accountId) + '?queue=420', False)

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getMatch(matchId, region = 'euw1'):
        match = RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/match/v3/matches/' + str(matchId))
        if match is False:
            return False
        timeline = RiotApi.__getTimeline(matchId, region)
        if timeline is False:
            return False
        match['timeline'] = timeline;
        return match

    @staticmethod
    def __getTimeline(matchId, region = 'euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/match/v3/timelines/by-match/' + str(matchId))

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getSummonerId(summonerName, region = 'euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/summoner/v3/summoners/by-name/' + str(summonerName))

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getActiveGame(accountId, region = 'euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/' + str(accountId))

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getRank(summonerId, region = 'euw1'):
        content = RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/league/v3/positions/by-summoner/' + str(summonerId))

        if content is False:
            return False

        ligaContent = None
        for liga in content:
            if liga['queueType'] == 'RANKED_SOLO_5x5':
                ligaContent = liga
                break

        if ligaContent is None:
            return False

        return {
            'rank': ligaContent['rank'],
            'tier': ligaContent['tier'],
            'leaguePoints': ligaContent['leaguePoints']
        }

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getListOfChallengers(region='euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/league/v3/challengerleagues/by-queue/RANKED_SOLO_5x5')

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getListOfMasters(region='euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/league/v3/masterleagues/by-queue/RANKED_SOLO_5x5')

    """False = Daten nicht vorhanden"""
    @staticmethod
    def getSummonerObject(summonerId, region='euw1'):
        return RiotApi.__readUrl('https://' + region + '.api.riotgames.com/lol/summoner/v3/summoners/' + str(summonerId))

    @staticmethod
    def test():
        # Nur zu debug zwecken
        print('start')

        #Stefans ID = 202859242
        #Simons ID = 204279396 <------------

        matches = RiotApi.getAllMatches(204279396)
        if matches is False:
            print('Keine Daten')
            return
        for match in matches['matches']:
            print (match['gameId'])
            gameId = match['gameId']
            match = RiotApi.getMatch(gameId)
            if (match is False):
                print('Keine Daten zu dem Spiel')
                continue
            print('Queue: ' + str(match['queueId']))

        #print(RiotApi.getAllMatches(202859242))
        #print(RiotApi.getMatch(3594086461))
        #content = RiotApi.getMatch(3594086461)
