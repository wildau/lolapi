import time
import threading
from sklearn import svm
from sklearn import datasets
from sklearn.externals import joblib
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import learning_curve
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import ElasticNet
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.linear_model import OrthogonalMatchingPursuit
from sklearn.linear_model import LogisticRegressionCV
import csv, os, sys
from riotApi import RiotApi
from exporter import Exporter
from exporter import ExporterVersionEnum
import couchdb
import database
import constant
import random
import json
import logging
import numpy as np

class Analyses:
    OLD_VIEW = False
    HEADERS_TO_SKIP = 1 if OLD_VIEW else 3
    PATH_TO_CSV = 'C:/Users/Stefan/Downloads/couchdb.csv' if OLD_VIEW else 'C:/Users/Stefan/Downloads/EXPORTER/'
    CONVERT_MAP = {'null': 0, 'blue': 1, 'red': 2}
    GAME_DURATION_ARRAY = [5, 10, 15, 20, 25, 30, 40, 50]
    STANDARD_ALGORITHM = LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1,
                                   class_weight=None, random_state=None, solver='liblinear', max_iter=100,
                                   multi_class='ovr', verbose=0, warm_start=False, n_jobs=1)
    debug = constant.debug
    _server = couchdb.Server(constant.COUCH_DB_SERVER_URL)
    _database = _server[constant.DATABASE_NAME]

    _champions = {};
    with open(os.path.dirname(__file__) + '/champions.json', 'r', encoding='utf8') as championFile:
        champions = json.load(championFile)
        for key in champions['data']:
            id = champions['data'][key]['key']
            championName = champions['data'][key]['name']
            _champions[championName] = int(id)
    _summonerSpells = {};
    with open(os.path.dirname(__file__) + '/summoner_spells.json', 'r', encoding='utf8') as summonerFile:
        summonerSpells = json.load(summonerFile)
        for key in summonerSpells['data']:
            id = summonerSpells['data'][key]['key']
            spellName = summonerSpells['data'][key]['name']
            _summonerSpells[spellName] = int(id)

    @staticmethod
    def __saveClassification(clf, filename):
        joblib.dump(clf, os.path.dirname(__file__) + '/' + filename + '.pkl')

    @staticmethod
    def __loadClassification(filename):
        return joblib.load(os.path.dirname(__file__) + '/' + filename + '.pkl')

    @staticmethod
    def __readFile(path):
        targets = []
        data = []
        with open(path, 'r') as csvFile:
            rowCount = 0
            reader = csv.reader(csvFile, delimiter=',')
            for row in reader:
                rowCount += 1
                if rowCount <= Analyses.HEADERS_TO_SKIP:
                    continue

                if row[0] == 'True':
                    targets.append(True)
                else:
                    targets.append(False)
                values = []
                for col in row[2:]:#Um das target und den EloString zu entfernen
                    value = str(col)
                    if value == 'True' or value == 'False':
                        value = bool(col)
                    elif type(col) is int:
                        value = float(col)
                    elif type(col) is float:
                        value = float(col)
                    elif type(col) is str:
                        value = str(col)
                        if value in Analyses.CONVERT_MAP:
                            value = Analyses.CONVERT_MAP[value]
                        elif value in Analyses._champions:
                            value = Analyses._champions[value]
                        elif value in Analyses._summonerSpells:
                            value = Analyses._summonerSpells[value]
                        value = float(value)
                    values.append(value)

                data.append(np.array(values))
        return (np.array(targets), np.array(data))

    @staticmethod
    def __getDataLikeInReadFile(content):
        target = None;
        data = []

        row = content.split('\n')[1].split(',')

        if row[0] == 'True':
            target = True
        else:
            target = False
        values = []
        for col in row[2:]:  # Um das target und den EloString zu entfernen
            value = str(col)
            if value == 'True' or value == 'False':
                value = bool(col)
            elif type(col) is int:
                value = float(col)
            elif type(col) is float:
                value = float(col)
            elif type(col) is str:
                value = str(col)
                if value in Analyses.CONVERT_MAP:
                    value = Analyses.CONVERT_MAP[value]
                elif value in Analyses._champions:
                    value = Analyses._champions[value]
                elif value in Analyses._summonerSpells:
                    value = Analyses._summonerSpells[value]
                value = float(value)
            values.append(value)

        data.append(values)

        return (target, np.array(data))

    @staticmethod
    def __fitAlgorithm(data, targets, clf, withNorm = False):
        #clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
        #clf = svm.SVC(gamma=0.001, C=100.)
        #clf = GaussianNB()
        #clf = LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1, class_weight=None, random_state=None, solver='liblinear',  max_iter=100, multi_class='ovr', verbose=0, warm_start=False, n_jobs=1)

        if withNorm:
            data = preprocessing.scale(data)

        clf.fit(data, targets)
        return clf

    @staticmethod
    def __test(targets, data, crossValidate=True, verbose = True):
        clfs = \
            [
                LogisticRegressionCV(),
                LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=1.0, fit_intercept=True, intercept_scaling=1,
                                   class_weight=None, random_state=None, solver='liblinear', max_iter=100,
                                   multi_class='ovr', verbose=0, warm_start=False, n_jobs=1),
            ]
        names = \
            [
                'LogisticRegressionCV',
                'LogisticRegression',
            ]

        if (crossValidate):
            print('Anlernen aller Algorithmen...')
            percentages = {}
            for name, clf in zip(names, clfs):
                percentages[name] = ''

                if verbose: print('Algorithmus wird angelernt...')

                scores = cross_val_score(clf, data, targets, cv=5)

                percentages[name] = "Accuracy: %0.2f (+/- %0.2f)" % (scores.mean() * 100, scores.std() * 2 * 100)
                if verbose: print(name + ": %0.2f (+/- %0.2f)" % (scores.mean() * 100, scores.std() * 2 * 100))

            print('Zusammenfassung:')
            for name in percentages:
                print(name + ': ' + percentages[name])
            return
        print('Wird gesampelt...')

        trainData, testData, trainTargets, testTargets = train_test_split(data, targets, test_size=0.1)
        countOfTestData = len(testData)

        print('Anlernen aller Algorithmen...')
        percentages = {}
        for name, clf in zip(names, clfs):
            percentages[name] = []
            for withNorm in [False, True]:
                if verbose: print('Algorithmus wird angelernt...')
                clf = Analyses.__fitAlgorithm(trainData, trainTargets, clf, withNorm)

                if verbose: print('Genauigkeit wird getestet...')
                results = clf.predict(testData)

                good = 0
                fail = 0
                for result, real in zip(results, testTargets):
                    if result == real:
                        good += 1
                    else:
                        fail += 1

                if verbose: print('Anzahl Daten: ' + str(len(trainData)))
                if verbose: print('Test-Daten:   ' + str(countOfTestData))
                if verbose: print('Richtig:      ' + str(good))
                if verbose: print('Falsch:       ' + str(fail))
                percentages[name].append(100 / countOfTestData * good)
                print(name + ' (' + ('Normalized)' if withNorm else 'Raw)       ') + ' %-Richtig:    ' + str(100 / countOfTestData * good) + '%')

        print('Zusammenfassung:')
        for name in percentages:
            percentage = percentages[name]
            print(name + ' (Raw)        %-Richtig:    ' + str(percentage[0]) + '%')
            print(name + ' (Normalized) %-Richtig:    ' + str(percentage[1]) + '%')

    @staticmethod
    def run(minutes = GAME_DURATION_ARRAY, test = False):
        print('Start Analyses')

        if Analyses.OLD_VIEW:
            print('Wird eingelesen...')
            targets, data = Analyses.__readFile(Analyses.PATH_TO_CSV)
            if test:
                Analyses.__test(targets, data)
            else:
                clf = Analyses.__fitAlgorithm(data, targets, Analyses.STANDARD_ALGORITHM)
                Analyses.__saveClassification(clf, 'classificationFile')
        else:
            path = Analyses.PATH_TO_CSV
            for filename in os.listdir(path):
                minute = int(filename.split('_')[3].split('Min')[0])
                if minute not in minutes: continue
                print('Starte/Lese Minute ' + str(minute) + ' ...')

                targets, data = Analyses.__readFile(path + filename)

                if test:
                    Analyses.__test(targets, data)
                else:
                    clf = Analyses.__fitAlgorithm(data, targets, Analyses.STANDARD_ALGORITHM)
                    Analyses.__saveClassification(clf, 'classificationFile' + str(minute))

        print('Fertig')
        exit(0)

    @staticmethod
    def predict(matchId, region = 'euw1'):
        logging.info('Starte Prediction...')

        logging.info('Matchdaten werden abgerufen...')
        database.saveMatch(matchId, int(database._startDate))

        mango = {'selector': {'gameId': {'$eq': matchId}}}
        result = list(Analyses._database.find(mango))
        if len(result) <= 0:
            logging.error('Matchdaten nicht im Server gefunden. Bitte versuchen Sie es später erneut...')
            print('Matchdaten nicht im Server gefunden. Bitte versuchen Sie es spaeter erneut...')
            return
        match = result[0]

        if Analyses.OLD_VIEW:
            if match['gameDuration'] / 60.0 < 20:
                logging.error('Das Spiel ist nicht 20 Minuten lang!')
                print('Das Spiel ist nicht 20 Minuten lang!')
                return

            result = Analyses._database.view('_design/design/_view/exporter-view', key=matchId)
            if len(result.rows) <= 0:
                logging.error('Matchdaten-View nicht im Server gefunden. Bitte versuchen Sie es später erneut...')
                print('Matchdaten-View nicht im Server gefunden. Bitte versuchen Sie es spaeter erneut...')
                return

            logging.info('Matchdaten werden verarbeitet...')
            target, data = Analyses.__getDataLikeInReadFile(Exporter.convertMatchToCSVString(result.rows[0]))

            logging.info('Klassifikation wird geladen...')
            clf = Analyses.__loadClassification('classificationFile')
            prediction = bool(clf.predict(data)[0])
            prob = clf.predict_proba(data)[0]

            message = 'Sieg für Team "'
            if prediction:
                message += 'Blau'
            else:
                message += 'Rot'
            message += '" mit einer Wahrscheinlichkeit von ' + str(int(round(max(prob) * 100))) + ' %'
            message += '. Tatsächlich hat ' + ('Blau' if target else 'Rot') + ' gewonnen.'

            logging.info(message)
            percentageBlueWin = str(int(round(max(prob) * 100)))
            if prediction is False:
                percentageBlueWin = str(int(round((min(prob)) * 100)))
            print('SUCCESS:' + percentageBlueWin + ':' + str(target))
        else:
            result = Analyses._database.view('_design/design/_view/exporter-view-v2', key=matchId)
            if len(result.rows) <= 0:
                logging.error('Matchdaten-View nicht im Server gefunden. Bitte versuchen Sie es später erneut...')
                print('Matchdaten-View nicht im Server gefunden. Bitte versuchen Sie es spaeter erneut...')
                return

            output = []
            for gameDurationInMin in Analyses.GAME_DURATION_ARRAY:
                logging.info('Starting for Minute ' + str(gameDurationInMin) + '...')

                if match['gameDuration'] / 60.0 < gameDurationInMin:
                    continue

                logging.info('Matchdaten werden verarbeitet...')
                target, data = Analyses.__getDataLikeInReadFile(Exporter.convertMatchToCSVString(result.rows[0], ExporterVersionEnum.CALCULATED_ROLES_NEW, gameDurationInMin))

                logging.info('Klassifikation wird geladen...')
                clf = Analyses.__loadClassification('classificationFile' + str(gameDurationInMin))
                prediction = bool(clf.predict(data)[0])
                prob = clf.predict_proba(data)[0]

                message = 'Minute ' + str(gameDurationInMin) + ' :'
                message += 'Sieg für Team "'
                if prediction:
                    message += 'Blau'
                else:
                    message += 'Rot'
                message += '" mit einer Wahrscheinlichkeit von ' + str(int(round(max(prob) * 100))) + ' %'
                message += '\nTatsächlich hat ' + ('Blau' if target else 'Rot') + ' gewonnen.'

                logging.info(message)
                percentageBlueWin = str(int(round(max(prob) * 100)))
                if prediction is False:
                    percentageBlueWin = str(int(round((min(prob)) * 100)))

                output.append('SUCCESS:' + percentageBlueWin + ':' + str(target) + ':' + str(gameDurationInMin))

            print('#'.join(output))