import time
import threading
from multiprocessing.dummy import Pool, Lock
from enum import Enum
import couchdb
import constant
import json
import os
import logging

debug = constant.debug
addUnnecessaryData = False
addHeaderForOrange = True # nur für CALCULATED_ROLES_NEW
exportPath = os.path.dirname(__file__) + '/EXPORTER/'

class ExporterVersionEnum(Enum):
    AGGREGATED_OLD = 1
    CALCULATED_ROLES_NEW = 2

class Exporter:
    _server = couchdb.Server(constant.COUCH_DB_SERVER_URL)
    _database = _server[constant.DATABASE_NAME]
    _roleArray = ['TOP', 'MIDDLE', 'JUNGLE', 'BOTTOM_ADC', 'BOTTOM_SUPPORT']

    def __init__(self, version : ExporterVersionEnum, gameDurationInMin = 20):
        self._content = []
        self._version = version
        self._gameDurationInMin = gameDurationInMin
        self._fails = 0
        self._errorInRoleCalculation = 0
        self._uncorrectRolesInJSON = 0
        self._gameNotLongEnough = 0
        self._lock = threading.Lock()
        self._champions = {};
        with open(os.path.dirname(__file__) + '/champions.json', 'r', encoding='utf8') as championFile:
            champions = json.load(championFile)
            for key in champions['data']:
                id = champions['data'][key]['key']
                championName = champions['data'][key]['name']
                self._champions[int(id)] = championName
        self._summonerSpells = {};
        with open(os.path.dirname(__file__) + '/summoner_spells.json', 'r', encoding='utf8') as summonerFile:
            summonerSpells = json.load(summonerFile)
            for key in summonerSpells['data']:
                id = summonerSpells['data'][key]['key']
                spellName = summonerSpells['data'][key]['name']
                self._summonerSpells[int(id)] = spellName

    def __generateColumnHeaders(self):
        str_list = []

        str_list.append('win')
        str_list.append('eloString')
        str_list.append('eloFloat')
        str_list.append('firstTower')
        str_list.append('firstDragon')
        str_list.append('firstBlood')

        if self._version == ExporterVersionEnum.AGGREGATED_OLD:
            arr = ['team1', 'team2']
            for participant in arr:
                str_list.append('countTowers_' + str(participant))
                str_list.append('countDragons_' + str(participant))
                str_list.append('countRiftHeralds_' + str(participant))
                str_list.append('countInhibitors_' + str(participant))
                str_list.append('xpPerMinDelta' + str(participant))
                str_list.append('goldPerMinDelta' + str(participant))
                str_list.append('creepsPerMinDelta' + str(participant))
                str_list.append('kills' + str(participant))
                str_list.append('assists' + str(participant))
                str_list.append('deaths' + str(participant))
                str_list.append('level' + str(participant))
                str_list.append('jungleMinionsKilled' + str(participant))
                str_list.append('totalGold' + str(participant))
                str_list.append('minionsKilled' + str(participant))
                str_list.append('yellowTrinket_' + str(participant))
                str_list.append('controlWard_' + str(participant))
                str_list.append('wardsKilled_' + str(participant))

        elif self._version == ExporterVersionEnum.CALCULATED_ROLES_NEW:
            teams = ['diff']
            for team in teams:
                str_list.append('totalTeamGold_' + team)
                str_list.append('countTowers_' + team)
                str_list.append('countDragons_' + team)
                str_list.append('countRiftHeralds_' + team)
                str_list.append('countInhibitors_' + team)
                for participant in Exporter._roleArray:
                    if addUnnecessaryData:
                        str_list.append('xpPerMinDelta_' + team + '_' + participant)
                        str_list.append('goldPerMinDelta_' + team + '_' + participant)
                        str_list.append('creepsPerMinDelta_' + team + '_' + participant)
                    str_list.append('kills_' + team + '_' + participant)
                    str_list.append('assists_' + team + '_' + participant)
                    str_list.append('deaths_' + team + '_' + participant)
                    str_list.append('level_' + team + '_' + participant)
                    str_list.append('jungleMinionsKilled_' + team + '_' + participant)
                    str_list.append('totalGold_' + team + '_' + participant)
                    str_list.append('minionsKilled_' + team + '_' + participant)
                    if addUnnecessaryData:
                        str_list.append('yellowTrinket_' + team + '_' + participant)
                        str_list.append('blueTrinket_' + team + '_' + participant)
                        str_list.append('controlWard_' + team + '_' + participant)
                        str_list.append('wardsKilled_' + team + '_' + participant)

        self._content.append(','.join(str_list))

        if self._version == ExporterVersionEnum.CALCULATED_ROLES_NEW and addHeaderForOrange:
            str_list = []

            str_list.append('discrete')
            str_list.append('discrete')
            str_list.append('continuous')
            str_list.append('discrete')
            str_list.append('discrete')
            str_list.append('discrete')

            teams = ['diff']
            for __ in teams:
                str_list.append('continuous')
                str_list.append('continuous')
                str_list.append('continuous')
                str_list.append('continuous')
                str_list.append('continuous')
                for __ in Exporter._roleArray:
                    if addUnnecessaryData:
                        str_list.append('continuous')
                        str_list.append('continuous')
                        str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    str_list.append('continuous')
                    if addUnnecessaryData:
                        str_list.append('continuous')
                        str_list.append('continuous')
                        str_list.append('continuous')
                        str_list.append('continuous')

            self._content.append('\n')
            self._content.append(','.join(str_list))

            str_list = []

            str_list.append('class')
            str_list.append('')
            str_list.append('')
            str_list.append('')
            str_list.append('')
            str_list.append('')

            teams = ['diff']
            for __ in teams:
                str_list.append('')
                str_list.append('')
                str_list.append('')
                str_list.append('')
                str_list.append('')
                for __ in Exporter._roleArray:
                    if addUnnecessaryData:
                        str_list.append('')
                        str_list.append('')
                        str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    str_list.append('')
                    if addUnnecessaryData:
                        str_list.append('')
                        str_list.append('')
                        str_list.append('')
                        str_list.append('')

            self._content.append('\n')
            self._content.append(','.join(str_list))

    def __addMatchToContent(self, row):
        try:
            gameId = row['key']
            match = row['value']
            str_list = []



            if self._version == ExporterVersionEnum.AGGREGATED_OLD:
                str_list.append(str(match['firstTower']))
                str_list.append(str(match['firstDragon']))
                str_list.append(str(match['firstBlood']))

                team1_xpPerMinDelta = 0
                team1_goldPerMinDelta = 0
                team1_creepsPerMinDelta = 0
                team1_kills = 0
                team1_assists = 0
                team1_deaths = 0
                team1_level = 0
                team1_jungleMinionsKilled = 0
                team1_totalGold = 0
                team1_minionsKilled = 0
                team1_yellowTrinket = 0
                team1_controlWard = 0
                team1_wardsKilled = 0

                team2_xpPerMinDelta = 0
                team2_goldPerMinDelta = 0
                team2_creepsPerMinDelta = 0
                team2_kills = 0
                team2_assists = 0
                team2_deaths = 0
                team2_level = 0
                team2_jungleMinionsKilled = 0
                team2_totalGold = 0
                team2_minionsKilled = 0
                team2_yellowTrinket = 0
                team2_controlWard = 0
                team2_wardsKilled = 0

                for index, participant in enumerate(match['participants']):
                    if 'xpPerMinDelta' not in participant:
                        return
                    if 'goldPerMinDelta' not in participant:
                        return
                    if 'creepsPerMinDelta' not in participant:
                        return

                    if index <= 4:
                        team1_xpPerMinDelta += participant['xpPerMinDelta']
                        team1_goldPerMinDelta += participant['goldPerMinDelta']
                        team1_creepsPerMinDelta += participant['creepsPerMinDelta']
                        team1_kills += participant['kills']
                        team1_assists += participant['assists']
                        team1_deaths += participant['deaths']
                        team1_level += participant['level']
                        team1_jungleMinionsKilled += participant['jungleMinionsKilled']
                        team1_totalGold += participant['totalGold']
                        team1_minionsKilled += participant['minionsKilled']
                        team1_yellowTrinket += participant['yellowTrinket']
                        team1_controlWard += participant['controlWard']
                        team1_wardsKilled += participant['wardsKilled']
                    else:
                        team2_xpPerMinDelta += participant['xpPerMinDelta']
                        team2_goldPerMinDelta += participant['goldPerMinDelta']
                        team2_creepsPerMinDelta += participant['creepsPerMinDelta']
                        team2_kills += participant['kills']
                        team2_assists += participant['assists']
                        team2_deaths += participant['deaths']
                        team2_level += participant['level']
                        team2_jungleMinionsKilled += participant['jungleMinionsKilled']
                        team2_totalGold += participant['totalGold']
                        team2_minionsKilled += participant['minionsKilled']
                        team2_yellowTrinket += participant['yellowTrinket']
                        team2_controlWard += participant['controlWard']
                        team2_wardsKilled += participant['wardsKilled']

                str_list.append(str(match['countTowers'][0]))
                str_list.append(str(match['countDragons'][0]))
                str_list.append(str(match['countRiftHeralds'][0]))
                str_list.append(str(match['countInhibitors'][0]))
                str_list.append(str(team1_xpPerMinDelta))
                str_list.append(str(team1_goldPerMinDelta))
                str_list.append(str(team1_creepsPerMinDelta))
                str_list.append(str(team1_kills))
                str_list.append(str(team1_assists))
                str_list.append(str(team1_deaths))
                str_list.append(str(team1_level))
                str_list.append(str(team1_jungleMinionsKilled))
                str_list.append(str(team1_totalGold))
                str_list.append(str(team1_minionsKilled))
                str_list.append(str(team1_yellowTrinket))
                str_list.append(str(team1_controlWard))
                str_list.append(str(team1_wardsKilled))

                str_list.append(str(match['countTowers'][1]))
                str_list.append(str(match['countDragons'][1]))
                str_list.append(str(match['countRiftHeralds'][1]))
                str_list.append(str(match['countInhibitors'][1]))
                str_list.append(str(team2_xpPerMinDelta))
                str_list.append(str(team2_goldPerMinDelta))
                str_list.append(str(team2_creepsPerMinDelta))
                str_list.append(str(team2_kills))
                str_list.append(str(team2_assists))
                str_list.append(str(team2_deaths))
                str_list.append(str(team2_level))
                str_list.append(str(team2_jungleMinionsKilled))
                str_list.append(str(team2_totalGold))
                str_list.append(str(team2_minionsKilled))
                str_list.append(str(team2_yellowTrinket))
                str_list.append(str(team2_controlWard))
                str_list.append(str(team2_wardsKilled))

            elif self._version == ExporterVersionEnum.CALCULATED_ROLES_NEW:
                if match['gameDurationInMin'] < self._gameDurationInMin:
                    with self._lock:
                        self._gameNotLongEnough += 1
                    raise ValueError('gameNotLongEnough')

                if match['rolesAreCorrect'] is False:
                    with self._lock:
                        self._errorInRoleCalculation += 1
                    raise ValueError('rolesAreCorrect: ' + str(match['rolesAreCorrect']))

                str_list.append(str(match['win']))
                str_list.append(str(match['eloString']))
                str_list.append(str(match['eloFloat']))

                minuteKey = str(self._gameDurationInMin)

                firstTower = match['firstTower']
                if match['firstTower'] == 'blue' and match['countTowers'][0][minuteKey] <= 0:
                    firstTower = 'null'
                if match['firstTower'] == 'red' and match['countTowers'][1][minuteKey] <= 0:
                    firstTower = 'null'
                str_list.append(firstTower)

                firstDragon = match['firstDragon']
                if match['firstDragon'] == 'blue' and match['countDragons'][0][minuteKey] <= 0:
                    firstDragon = 'null'
                if match['firstDragon'] == 'red' and match['countDragons'][1][minuteKey] <= 0:
                    firstDragon = 'null'
                str_list.append(firstDragon)

                #FIRST BLOOD
                firstBlood = match['firstBlood']
                if match['firstBlood'] == 'blue':
                    team_id = 0
                if match['firstBlood'] == 'red':
                    team_id = 1

                if match['firstBlood'] != 'null':
                    participants = [];
                    for role in Exporter._roleArray:
                        for index, participant in enumerate(match['participants']):
                            if (index < 5 and team_id == 0) or (index >= 5 and team_id == 1):
                                continue
                            if participant['calculatedRole'] == role:
                                participants.append(participant)
                                break

                    kills = 0
                    for participant in participants:
                        kills += participant['kills'][minuteKey]

                    if kills <= 0:
                        firstBlood = 'null'

                str_list.append(firstBlood)
                # FIRST BLOOD

                totalTeamGold = [0, 0]
                for index, participant in enumerate(match['participants']):
                    if index < 5:
                        totalTeamGold[0] += participant['totalGold'][minuteKey]
                    else:
                        totalTeamGold[1] += participant['totalGold'][minuteKey]

                jsonRolesAreCorrect = True
                for _ in ['diff']:
                    team_id = 0
                    str_list.append(str(totalTeamGold[team_id] - totalTeamGold[team_id+1]))
                    str_list.append(str(match['countTowers'][team_id][minuteKey] - match['countTowers'][team_id+1][minuteKey]))
                    str_list.append(str(match['countDragons'][team_id][minuteKey] - match['countDragons'][team_id+1][minuteKey]))
                    str_list.append(str(match['countRiftHeralds'][team_id][minuteKey] - match['countRiftHeralds'][team_id+1][minuteKey]))
                    str_list.append(str(match['countInhibitors'][team_id][minuteKey] - match['countInhibitors'][team_id+1][minuteKey]))

                    participants1 = [];
                    for role in Exporter._roleArray:
                        for index, participant in enumerate(match['participants']):
                            if (index < 5 and team_id == 0) or (index >=5 and team_id == 1):
                                continue
                            if participant['calculatedRole'] == role:
                                participants1.append(participant)
                                break

                    team_id = 1
                    participants2 = [];
                    for role in Exporter._roleArray:
                        for index, participant in enumerate(match['participants']):
                            if (index < 5 and team_id == 0) or (index >= 5 and team_id == 1):
                                continue
                            if participant['calculatedRole'] == role:
                                participants2.append(participant)
                                break

                    for participant1 in participants1:
                        participant2 = None
                        for par in participants2:
                            if participant1['calculatedRole'] == par['calculatedRole']:
                                participant2 = par
                                break

                        lane = str(participant1['lane'])
                        if lane == 'BOTTOM':
                            if str(participant1['role']) == 'DUO_CARRY':
                                lane += '_ADC'
                            else:
                                lane += '_SUPPORT'

                        if lane != participant['calculatedRole']:
                            jsonRolesAreCorrect = False

                        if addUnnecessaryData:
                            if 'xpPerMinDelta' not in participant:
                                raise ValueError('xpPerMinDelta: not found')
                            str_list.append(str(participant['xpPerMinDelta']))
                            if 'goldPerMinDelta' not in participant:
                                raise ValueError('goldPerMinDelta: not found')
                            str_list.append(str(participant['goldPerMinDelta']))
                            if 'creepsPerMinDelta' not in participant:
                                raise ValueError('creepsPerMinDelta: not found')
                            str_list.append(str(participant['creepsPerMinDelta']))
                        str_list.append(str(participant1['kills'][minuteKey] - participant2['kills'][minuteKey]))
                        str_list.append(str(participant1['assists'][minuteKey] - participant2['assists'][minuteKey]))
                        str_list.append(str(participant1['deaths'][minuteKey] - participant2['deaths'][minuteKey]))
                        str_list.append(str(participant1['level'][minuteKey] - participant2['level'][minuteKey]))
                        str_list.append(str(participant1['jungleMinionsKilled'][minuteKey] - participant2['jungleMinionsKilled'][minuteKey]))
                        str_list.append(str(participant1['totalGold'][minuteKey] - participant2['totalGold'][minuteKey]))
                        str_list.append(str(participant1['minionsKilled'][minuteKey] - participant2['minionsKilled'][minuteKey]))
                        if addUnnecessaryData:
                            str_list.append(str(participant['yellowTrinket'][minuteKey]))
                            str_list.append(str(participant['blueTrinket'][minuteKey]))
                            str_list.append(str(participant['controlWard'][minuteKey]))
                            str_list.append(str(participant['wardsKilled'][minuteKey]))

                if jsonRolesAreCorrect is False:
                    with self._lock:
                        self._uncorrectRolesInJSON += 1

            with self._lock:
                self._content.append('\n' + ','.join(str_list))
        except Exception as exc:
            with self._lock:
                self._fails += 1
            if type(exc) is ValueError and (exc.args[0] == 'gameNotLongEnough' or exc.args[0] == 'rolesAreCorrect: False'):
                pass
            else:
                logging.info(exc)

    @staticmethod
    def convertMatchToCSVString(match, version : ExporterVersionEnum = ExporterVersionEnum.AGGREGATED_OLD, gameDurationInMin = 20):
        exporter = Exporter(version, gameDurationInMin)
        exporter.__addMatchToContent(match)
        return exporter._content[0]

    @staticmethod
    def run(version : ExporterVersionEnum = ExporterVersionEnum.AGGREGATED_OLD, gameDurationInMinArray = [20]):
        print('Exporter... (lade herunter)')
        _threadPool = Pool(50)

        if version == ExporterVersionEnum.AGGREGATED_OLD:
            result = Exporter._database.view('_design/design/_view/exporter-view')
        elif version == ExporterVersionEnum.CALCULATED_ROLES_NEW:
            result = Exporter._database.view('_design/design/_view/exporter-view-v2')

        print(str(result.total_rows) + ' Dokumente heruntergeladen. Werden jetzt verarbeitet...') # Äquivalent zu str(len(result.rows))

        if version == ExporterVersionEnum.AGGREGATED_OLD:
            print('*********************************************************')
            print('Exporter startet ...')

            exporter = Exporter(version, gameDurationInMinArray[0])

            exporter.__generateColumnHeaders()
            _threadPool.map(exporter.__addMatchToContent, result.rows)

            with open(exportPath + 'py_exporter_' + 'v' + str(exporter._version.value) + '_20Min_' + str(len(exporter._content) - 1) + 'Docs.csv', 'w') as file:
                file.writelines(exporter._content)

            print('#########################################')
            print('All Fails: ' + str(exporter._fails))
            print('Heruntergeladene Dokumente: ' + str(result.total_rows))
            print('Davon letztendlich exportierte Zeilen: ' + str(len(exporter._content) - 1))
            print('Fertig')
        elif version == ExporterVersionEnum.CALCULATED_ROLES_NEW:
            for gameDurationInMin in gameDurationInMinArray:
                print('*********************************************************')
                print('Exporter startet für Minute ' + str(gameDurationInMin) + '...')

                exporter = Exporter(version, gameDurationInMin)

                exporter.__generateColumnHeaders()
                _threadPool.map(exporter.__addMatchToContent, result.rows)

                with open(exportPath + 'py_exporter_' + 'v' + str(exporter._version.value) + '_' + str(gameDurationInMin) + 'Min_' + str(len(exporter._content) - 2) + 'Docs.csv', 'w') as file:
                    file.writelines(exporter._content)

                print('#########################################')
                print('All Fails: ' + str(exporter._fails))
                print('Spiele bei denen die Rollen nicht gleichmäßig verteilt wurden: ' + str(exporter._errorInRoleCalculation))
                print('Spiele in denen die Rolle in der API (JSON) falsch war: ' + str(exporter._uncorrectRolesInJSON))
                print('Unexpected Fails: ' + str(exporter._fails - exporter._errorInRoleCalculation - exporter._gameNotLongEnough))
                print('Heruntergeladene Dokumente: ' + str(result.total_rows))
                print('Davon letztendlich exportierte Zeilen: ' + str(len(exporter._content) - 2))
                print('Zu kurze Spiele: ' + str(exporter._gameNotLongEnough))
                print('Fertig')

        print('Alle exportiert')
        exit(0)