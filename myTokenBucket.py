from collections import deque
import time


class MyTokenBucket(object):
    def __init__(self):
        self._queue1 = self.TokenQueue(20, 1) # Jede Sekunde 20 Abfragen
        self._queue2 = self.TokenQueue(100, 120) # Alle 120 Sekunden (2 Minuten) 100 Abfragen

    # Wann das nächste Mal eine Abfrage getätigt werden darf
    def secondsTillNextConsume(self):
        seconds1 = self._queue1.secondsTillNextConsume()
        seconds2 = self._queue2.secondsTillNextConsume()
        return seconds1 if seconds1 > seconds2 else seconds2

    def consume(self):
        seconds = time.time()
        self._queue1.put(seconds)
        self._queue2.put(seconds)

    def fillUp(self):
        self._queue1.fillUp()
        self._queue2.fillUp()

    class TokenQueue(object):
        def __init__(self, capacity, fill_rate):
            self._capacity = capacity
            self._fill_rate = float(fill_rate)
            self._queue = deque()

        def secondsTillNextConsume(self):
            # Wenn Queue noch nicht voll, dann darf sofort eine Abfrage getätigt werden
            if len(self._queue) < self._capacity:
                return 0

            item = self._queue[0]
            spendTime = time.time() - item
            if spendTime > self._fill_rate:
                # Wenn ältestes Item in der Queue älter ist als die angegebenen Sekunden, dann darf sofort eine Abfrage getätigt werden
                self._queue.popleft()
                return 0
            else:
                waitTime = self._fill_rate - spendTime
                return 1 if waitTime < 1 else waitTime # nur zur sicherheit dass wirklich gewartet wird und nicht auf 0 gerundet

        def put(self, seconds):
            self._queue.append(seconds)

        def fillUp(self):
            seconds = time.time()
            self._queue.clear()
            while len(self._queue) < self._capacity:
                self._queue.append(seconds)