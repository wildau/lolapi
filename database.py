import os
import couchdb
from multiprocessing.dummy import Pool, Lock, Queue
import datetime
import operator
import threading
from riotApi import RiotApi
from eloSortEnum import EloSortEnum
import constant
import random
import logging

debug = constant.debug
_badAccounts = Queue()
_summonerRank = dict()
_server = couchdb.Server(constant.COUCH_DB_SERVER_URL)
_database = _server[constant.DATABASE_NAME]
_startDate = datetime.datetime(2018, 10, 9).timestamp()
_endDate = datetime.datetime(2018, 12, 12).timestamp()
_lock = Lock()
_elos = {'BRONZE': 1, 'SILVER': 6, 'GOLD': 11, 'PLATINUM': 16, 'DIAMOND': 21, 'MASTER': 26, 'CHALLENGER': 31}
_sorted_elos = sorted(_elos.items(), key=lambda kv: kv[1])
_rank = {'I': 4, 'II': 3, 'III': 2, 'IV': 1, 'V': 0}
_searchForMasterAndChallengerGames = True # nicht auf False setzen sondern lieber in 'constant' ändern

class MyThread(threading.Thread):
    def __init__(self, match):
        threading.Thread.__init__(self)
        self.match = match

    def run(self):
        saveMatch(self.match['gameId'], int(str(self.match['timestamp'])[:-3]))

def addMatchElo(match):
    elo = []
    for player in match['participantIdentities']:
        summonerId = player['player']['summonerId']
        if summonerId in _summonerRank:
            elo.append(_summonerRank[summonerId])
        else:
            playerRank = RiotApi.getRank(summonerId)
            if playerRank:
                elo.append(_elos.get(playerRank['tier']) + _rank.get(playerRank['rank']))
                _summonerRank[summonerId] = _elos.get(playerRank['tier']) + _rank.get(playerRank['rank'])

    if len(elo) != 0:
        eloFloat = sum(elo) / len(elo)
        for key, value in _sorted_elos:
            if eloFloat < value:
                break
            eloString = key
    else:
        eloFloat = 0.0
        eloString = 'UNRANKED'
    match['eloFloat'] = eloFloat
    match['eloString'] = eloString
    return match

'''Hier ist matchId tatsächlich eine ID'''
def saveMatch(matchId, gameCreation):
    mango = {'selector': {'gameId': {'$eq': matchId}}, 'fields': ['gameId']}
    timestamp = int(str(gameCreation))

    if timestamp >= _startDate and timestamp <= _endDate:
        if len(list(_database.find(mango))) == 0:
            match = RiotApi.getMatch(matchId)
            if match:
                match = addMatchElo(match)
                with _lock:
                    if len(list(_database.find(mango))) == 0:
                        try:
                            _database.save(match)
                            logging.info('saved ' + match['eloString'])
                        except couchdb.HTTPError:
                            logging.error('HTTP error')
                        except couchdb.ServerError:
                            logging.error('server error')

def __saveMatches(accountId):
    if accountId not in _badAccounts.queue:
        matches = RiotApi.getAllMatches(accountId)
        if matches:
            threads = []
            for match in matches['matches']:
                timestamp = int(str(match['timestamp'])[:-3])
                if timestamp >= _startDate and timestamp <= _endDate:
                    thread = MyThread(match)
                    thread.daemon = True
                    threads.append(thread)
                    thread.start()

            for thread in threads:
                thread.join()
        _badAccounts.put(accountId)

def __getLeastElo():
    result = _database.view('_design/design/_view/liga-view', group=True)

    map = {}
    for key, value in _elos.items():
        map[key] = 0

    for row in result.rows:
        if row.key is not None:
            map[row.key] = row.value

    map['MASTER'] += map['CHALLENGER']
    del map['CHALLENGER']

    if constant.includeChallengerAndMasterInSearch is False or _searchForMasterAndChallengerGames is False:
        del map['MASTER']

    map = sorted(map.items(), key=lambda kv: kv[1])

    return map[0][0]

def master(eloSortEnum=None):
    logging.info('START MASTER WITH ' + str(eloSortEnum))
    logging.info('PID: ' + str(os.getpid()))
    _threadPool = Pool(2)
    _badMatchIds = []
    _badSummonerIds = []

    while True:
        searchChallengerLeague = False

        if eloSortEnum is EloSortEnum.HIGH:
            mango = {'selector': {'gameId': {'$nin': _badMatchIds}}, 'sort': [{'eloFloat': 'desc'}], 'limit': 1}
        else:
            if eloSortEnum is EloSortEnum.LOW:
                mango = {'selector': {'gameId': {'$nin': _badMatchIds}}, 'sort': [{'eloFloat': 'asc'}], 'limit': 1}
            else:
                if eloSortEnum is EloSortEnum.EVEN:
                    leastElo = __getLeastElo()
                    logging.info('Now Searching for Elo: ' + leastElo)
                    if leastElo == 'MASTER':
                        searchChallengerLeague = True
                    else:
                        mango = {'selector': {'gameId': {'$nin': _badMatchIds}, 'eloString': {'$eq': leastElo}},
                                 'sort': [{'gameId': 'desc'}], 'limit': 1}
                else:
                    mango = {'selector': {'gameId': {'$nin': _badMatchIds}}, 'sort': [{'gameId': 'desc'}], 'limit': 1}

        if searchChallengerLeague:
            challengers = RiotApi.getListOfChallengers()
            masters = RiotApi.getListOfMasters()

            if challengers and master:
                entriesOfSummonerIds = [x['playerOrTeamId'] for x in challengers['entries']]
                entriesOfSummonerIds.extend([x['playerOrTeamId'] for x in masters['entries']])

                entriesOfSummonerIds = [x for x in entriesOfSummonerIds if x not in _badSummonerIds]

                if len(entriesOfSummonerIds) <= 0:
                    database._searchForMasterAndChallengerGames = False
                    continue

                sampleSize = 50
                if len(entriesOfSummonerIds) < sampleSize:
                    sampleSize = len(entriesOfSummonerIds)

                entriesOfSummonerIds = random.sample(entriesOfSummonerIds, sampleSize)

                accountIds = []
                for summonerId in entriesOfSummonerIds:
                    summonerObject = RiotApi.getSummonerObject(summonerId)
                    if summonerObject:
                        accountIds.append(summonerObject['accountId'])

                _threadPool.map(__saveMatches, accountIds)
                _badSummonerIds.extend(entriesOfSummonerIds)
        else:
            startMatch = list(_database.find(mango))
            if len(startMatch) != 0:
                startMatch = startMatch[0]
            else:
                startMatch = RiotApi.getMatch(RiotApi.getAllMatches(204279396)['matches'][0]['gameId'])

            accountIds = [participant['player']['accountId'] for participant in startMatch['participantIdentities']]
            _threadPool.map(__saveMatches, accountIds)
            _badMatchIds.append(startMatch['gameId'])
