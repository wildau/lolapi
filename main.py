import constant
from database import master
from eloSortEnum import EloSortEnum
from exporter import Exporter
from exporter import ExporterVersionEnum
from analyses import Analyses
import sys

if constant.startOnLocalhostToCollectData:
    master(EloSortEnum.EVEN)
    sys.exit(0)

if constant.useInWebsite:
    input = sys.argv[1]
    input = int(input)
    Analyses.predict(matchId=input)
    #https://matchhistory.na.leagueoflegends.com/en/#match-details/EUW1/3634760358/204279396?tab=overview
    sys.exit(0)

#Exporter.run(ExporterVersionEnum.AGGREGATED_OLD)
#sys.exit(0)
#Exporter.run(ExporterVersionEnum.CALCULATED_ROLES_NEW, [5, 10, 15, 20, 25, 30, 40, 50])
#sys.exit(0)
#Analyses.run(minutes = [20], test = True)
#sys.exit(0)

#3659895959 100% aus sicht von simon gewonnen (verloren) AUFNAHME VORHANDEN
#3662465560 100% verloren (verloren)
#3662524969 100% gewonnen (gewonnen) AUFNAHME VORHANDEN
#3662566913 92 % verloren (verloren) AUFNAHME VORHANDEN
