import logging

startOnLocalhostToCollectData = False
useInWebsite = False
includeChallengerAndMasterInSearch = True
debug = True
COUCH_DB_SERVER_URL = 'http://lolapi:QQ7dqcXL67YS@hirnstorm.de:5984'
DATABASE_NAME = 'lol_api_test' if debug else 'lol_api_euw_10'

if startOnLocalhostToCollectData:
    debug = False
    COUCH_DB_SERVER_URL = 'http://lolapi:QQ7dqcXL67YS@localhost:5984'
    DATABASE_NAME = 'lol_api_euw_10'

if useInWebsite:
    debug = False
    COUCH_DB_SERVER_URL = 'http://lolapi:QQ7dqcXL67YS@localhost:5984'
    DATABASE_NAME = 'lol_api_test'

if debug:
    logging.basicConfig(format='%(asctime)s\t%(levelname)s\t%(message)s', datefmt='%Y-%m-%d\t%H:%M:%S',
                        level=logging.DEBUG)
else:
    logging.basicConfig(filename='event.log', filemode='w', format='%(asctime)s\t%(levelname)s\t%(message)s',
                        datefmt='%Y-%m-%d\t%H:%M:%S', level=logging.INFO)
